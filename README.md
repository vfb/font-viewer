# 字体查看器
![输入图片说明](iconfont.jpg)
#### 介绍
![输入图片说明](iconfont.png)直接查看字体文件或系统里的字体，，单文件，小巧绿色。

#### 软件说明
[点此打开：本软件视频演示](https://www.douyin.com/video/7139865051271286024)

这是为了查看字体的绿色小软件，就单单一个EXE文件，不需要任何支持库，可以查看字体文件或系统里的字体。

主要是为了查看从 https://www.iconfont.cn/  网站下载来的图标字体，然后可以获取字体值。可以查看这个字体里的所有字体，同时这个软件是开源的，源码请进  VisualFreeBasic 编程QQ群里下载。QQ号见VisualFreeBasic 编程软件。

#### 安装教程

1.  [解压版下载](https://yfvb.lanzouy.com/iugZs0b3pesh/)
